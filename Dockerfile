FROM python:3.7

COPY . /app
WORKDIR /app

RUN pip install -r requirements.txt

RUN python manage.py makemigrations

RUN python manage.py migrate

RUN python manage.py collectstatic --noinput