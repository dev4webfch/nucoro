docker-compose exec django-restfull "python manage.py makemigrations"
docker-compose exec django-restfull "python manage.py migrate"
docker-compose exec django-restfull "python manage.py createsuperuser --noinput"