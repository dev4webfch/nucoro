from rest_framework import serializers
from api.models import Currency, CurrencyExchangeRate

class CurrencySerializer(serializers.ModelSerializer):

    class Meta:
        model = Currency
        fields = ['id','code', 'name', 'symbol']
    
class CurrencyExchangeRateSerializer(serializers.ModelSerializer):

    source_currency = serializers.SlugRelatedField(
        many=False,
        read_only=False,
        slug_field='code',
        queryset = Currency.objects.all()
     )

    exchanged_currency = serializers.SlugRelatedField(
        many=False,
        read_only=False,
        slug_field='code',
        queryset = Currency.objects.all()
     )

    # source_currency = CurrencySerializer(read_only=True)
    # exchanged_currency = CurrencySerializer(read_only=True)

    class Meta:
        model = CurrencyExchangeRate
        fields = ['id','source_currency','exchanged_currency','valuation_date', 'rate_value']


class CurrencyExchangeRateDatesSerializer(serializers.Serializer):

    source_currency = serializers.SlugRelatedField(
        many=False,
        read_only=False,
        slug_field='code',
        queryset = Currency.objects.all()
     )
    date_from = serializers.DateField(required=True)
    date_to = serializers.DateField(required=True)

class CurrencyExchangeRateAmountSerializer(serializers.Serializer):

    source_currency = serializers.CharField(max_length=3, required=True)
    amount = serializers.DecimalField(decimal_places=6, max_digits=18, required=True)
    exchanged_currency = serializers.CharField(max_length=3, required=True)

class CurrencyExchangeRateTimeWeightedSerializer(serializers.Serializer):

    source_currency = serializers.CharField(max_length=3,required=True)
    amount =  serializers.DecimalField(decimal_places=6, max_digits=18, required=True)
    exchanged_currency = serializers.CharField(max_length=3,required=True)
    start_date = serializers.DateField(required=True)

