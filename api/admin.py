from django.contrib import admin
import random
from api.models import Currency, CurrencyExchangeRate
# Register your models here.

def generate_random_data(modeladmin, request, queryset):
    curs = Currency.objects.all()
    for cur in curs:
        for cur2 in curs:
            #val =
            pass




@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display=['id','code','name','symbol']


@admin.register(CurrencyExchangeRate)
class CurrencyExchangeRateAdmin(admin.ModelAdmin):
    list_display=['id', 'source_currency','exchanged_currency','valuation_date', 'rate_value']
