from django.test import TestCase, Client
from api.models import Currency,  CurrencyExchangeRate
from api.serializers import CurrencySerializer, CurrencyExchangeRateSerializer
import json

# Create your tests here.


class TestCustomEndpoints(TestCase):

    def setUp(self):
        self.euro = Currency.objects.create(
            code="EUR", name="Euro", symbol="€")
        self.dolar = Currency.objects.create(
            code="USD", name="Dolar", symbol="$")
        self.euroEx = CurrencyExchangeRate.objects.create(
            source_currency=self.euro, exchanged_currency=self.euro, valuation_date='2021-05-12', rate_value=1)
        self.euroEx2 = CurrencyExchangeRate.objects.create(
            source_currency=self.euro, exchanged_currency=self.euro, valuation_date='2021-05-11', rate_value=1.2)
        self.euroEx3 = CurrencyExchangeRate.objects.create(
            source_currency=self.euro, exchanged_currency=self.euro, valuation_date='2021-05-10', rate_value=1.5)
        self.dolarEx = CurrencyExchangeRate.objects.create(
            source_currency=self.euro, exchanged_currency=self.dolar, valuation_date='2021-05-12', rate_value=0.8)
        self.dolarEx2 = CurrencyExchangeRate.objects.create(
            source_currency=self.euro, exchanged_currency=self.dolar, valuation_date='2021-05-11', rate_value=1.8)
        self.dolarEx3 = CurrencyExchangeRate.objects.create(
            source_currency=self.euro, exchanged_currency=self.dolar, valuation_date='2021-05-10', rate_value=2.0)

    def test_calculate_amount(self):
        resp = self.client.post('/api/currenciesExchanges/calculate_amount/', {
            "source_currency": "EUR",
            "amount": "1000",
            "exchanged_currency": "USD"
        }, content_type='application/json')
        self.assertEqual(resp.status_code, 200)

    def test_time_weighted(self):
        resp = self.client.post('/api/currenciesExchanges/time_weighted/', {
            "source_currency": "EUR",
            "amount": "1000",
            "exchanged_currency": "EUR",
            "start_date": "2021-02-10"
        }, content_type='application/json')
        self.assertEqual(resp.status_code, 200)

    def test_beetweenDates(self):
        resp = self.client.post('/api/currenciesExchanges/beetweenDates/',
        {"source_currency": "EUR", "date_from": "2021-02-10", "date_to": "2021-02-12"}, content_type='application/json')
        respc = json.loads(resp.content.decode("utf-8"))
        self.assertEqual(resp.status_code, 200)
