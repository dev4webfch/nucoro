from django.test import TestCase
from api.models import Currency,  CurrencyExchangeRate
from api.tasks import get_exchange_rate_data, get_exchange_rate_cron
from exchange.celery import app

# Create your tests here.
class TestTasksEndpoints(TestCase):

    def setUp(self):
        self.euros = Currency.objects.all()
        self.exchanges = CurrencyExchangeRate.objects.all()

    def test_load_from_cron(self):
        # get_exchange_rate_cron().s().delay(1)
        self.assertEqual(self.euros.count(), 0)
        self.assertEqual(self.exchanges.count(), 0)

    def test_load_from_event(self):
        # app.send_task()
        # get_exchange_rate_data().delay(1)
        self.assertEqual(self.euros.count(), 0)
        self.assertEqual(self.exchanges.count(), 0)