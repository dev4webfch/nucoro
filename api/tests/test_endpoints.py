from django.test import TestCase, Client
from api.models import Currency,  CurrencyExchangeRate
from api.serializers import CurrencySerializer, CurrencyExchangeRateSerializer
import json
# Create your tests here.
class TestBasicEndpoints(TestCase):

    def setUp(self):
        self.euro = Currency.objects.create(code="EUR", name="Euro", symbol="€")
        self.dolar = Currency.objects.create(code="USD", name="Dolar", symbol="$")
        self.euroEx = CurrencyExchangeRate.objects.create(source_currency=self.euro,exchanged_currency=self.euro,valuation_date='2021-05-12',rate_value=1)
        self.dolarEx = CurrencyExchangeRate.objects.create(source_currency=self.euro,exchanged_currency=self.dolar,valuation_date='2021-05-12',rate_value=0.8)

    def test_create_currency(self):
        resp = self.client.post('/api/currencies/', { "code": "GBP","name": "Pound","symbol": "GBP"})
        self.assertEqual(resp.status_code, 201)

    def test_update_currency(self):
        resp = self.client.put('/api/currencies/' + str(self.euro.id) + '/', { "code": "EUR","name": "EURO","symbol": "€"}, content_type='application/json')
        self.assertEqual(resp.status_code, 200)

    def test_list_currency(self):
        resp = self.client.get('/api/currencies/', content_type='application/json')
        self.assertEqual(resp.status_code, 200)

    def test_get_currency(self):
        resp = self.client.get('/api/currencies/' + str(self.euro.id) + '/', content_type='application/json')
        respc = json.loads(resp.content.decode("utf-8"))
        self.assertEqual(respc['id'], self.euro.id)
        self.assertEqual(resp.status_code, 200)

    def test_delete_currency(self):
        resp = self.client.delete('/api/currencies/' + str(self.euro.id) + '/', content_type='application/json')
        self.assertEqual(resp.status_code, 204)



    def test_create_CurrencyExchangeRate(self):
        resp = self.client.post('/api/currenciesExchanges/', { "source_currency": "EUR",
        "exchanged_currency": "EUR",
        "valuation_date": "2021-05-12",
        "rate_value": "2.500000"})
        self.assertEqual(resp.status_code, 201)

    def test_update_CurrencyExchangeRate(self):
        resp = self.client.put('/api/currenciesExchanges/' + str(self.euroEx.id) + '/', {"source_currency": "EUR",
        "exchanged_currency": "EUR",
        "valuation_date": "2021-05-12",
        "rate_value": "1.50"}, content_type='application/json')
        self.assertEqual(resp.status_code, 200)

    def test_list_CurrencyExchangeRate(self):
        resp = self.client.get('/api/currenciesExchanges/', content_type='application/json')
        self.assertEqual(resp.status_code, 200)

    def test_get_CurrencyExchangeRate(self):
        resp = self.client.get('/api/currenciesExchanges/' + str(self.euroEx.id) + '/', content_type='application/json')
        respc = json.loads(resp.content.decode("utf-8"))
        self.assertEqual(respc['id'], self.euroEx.id)
        self.assertEqual(resp.status_code, 200)

    def test_delete_CurrencyExchangeRate(self):
        resp = self.client.delete('/api/currenciesExchanges/' + str(self.euroEx.id) + '/', content_type='application/json')
        self.assertEqual(resp.status_code, 204)