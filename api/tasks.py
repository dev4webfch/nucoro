import requests
import datetime
import json
# from celery.task.schedules import crontab
from celery.decorators import periodic_task, task
from api.models import Currency, CurrencyExchangeRate

URL_EXCHANGE_LAST='http://data.fixer.io/api/latest'

@periodic_task(run_every=datetime.timedelta(minutes=1), name="get_exchange_rate_cron", ignore_result=True)
def get_exchange_rate_cron(source_currency=None, exchanged_currency=None, valuation_date=None, provider=None):
    resp = requests.get(url=URL_EXCHANGE_LAST, params={"access_key": "026195b930891132766d2be478ff31d4", "format": 2,
    "base": "EUR", "symbols":"EUR,GBP,CHF,USD"})
    base = resp.json()['base']
    date = resp.json()['date']
    rates_values = resp.json()['rates']
    for key, value in rates_values.items():
        cure = Currency.objects.filter(code=base).exists()
        if(not cure):
            ncur = Currency()
            ncur.code = base
            ncur.name = base
            ncur.symbol = base
            ncur.save()

    cure = Currency.objects.filter(code=base).exists()
    if(not cure):
        ncur = Currency()
        ncur.code = base
        ncur.name = base
        ncur.symbol = base
        ncur.save()
    else:
        base = Currency.objects.get(code=base)
    
    for key, value in rates_values.items():
        exch = Currency.objects.get(code=base)
        exchanges = CurrencyExchangeRate.objects.filter(source_currency__code=base, exchanged_currency__code=exch).exists()
        if(not exchanges):
            cure = Currency.objects.filter(code=key).exists()
            if(not cure):
                ncur = Currency()
                ncur.code = base
                ncur.name = base
                ncur.symbol = base
                ncur.save()
            else:
                cure = Currency.objects.get(code=base)
            curEx = CurrencyExchangeRate()
            curEx.exchanged_currency = exch
            curEx.rate_value = value
            curEx.valuation_date = date
            curEx.source_currency = base
            curEx.save()
        else:
            exchange = CurrencyExchangeRate.objects.get(source_currency__code=base, exchanged_currency__code=exch)
            exchange.rate_value = value
            exchange.valuation_date = date
            exchange.save()
        


@task(name="get_exchange_rate_data")
def get_exchange_rate_data(source_currency=None, exchanged_currency=None, valuation_date=None, provider=None):
    # the json providers format are much relative to create the data adapter function for each element
    if(provider=='Fixer'):
        resp = requests.get(url=URL_EXCHANGE_LAST, params={"access_key": "026195b930891132766d2be478ff31d4", "format": 2,
        "base": "EUR", "symbols":"EUR,GBP,CHF,USD"})
        base = resp.json()['base']
        date = resp.json()['date']
        rates_values = resp.json()['rates']
        for key, value in rates_values.items():
            cure = Currency.objects.filter(code=base).exists()
            if(not cure):
                ncur = Currency()
                ncur.code = base
                ncur.name = base
                ncur.symbol = base
                ncur.save()
        for key, value in rates_values.items():
            curEx = CurrencyExchangeRate()
            curEx.exchanged_currency = key
            curEx.rate_value = value
            curEx.valuation_date = date
            curEx.source_currency = base
            curEx.save()
    if(provider=='Mock:'):
        data=None
        with open('mock.json') as json_file:
            data = json.load(json_file)
        base = data['base']
        date = data['date']
        rates_values = data['rates']
        for key, value in rates_values.items():
            cure = Currency.objects.filter(code=base).exists()
            if(not cure):
                ncur = Currency()
                ncur.code = base
                ncur.name = base
                ncur.symbol = base
                ncur.save()
        for key, value in rates_values.items():
            curEx = CurrencyExchangeRate()
            curEx.exchanged_currency = key
            curEx.rate_value = value
            curEx.valuation_date = date
            curEx.source_currency = base
            curEx.save()