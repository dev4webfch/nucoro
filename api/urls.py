from api.views import CurrencyViewSet, CurrencyExchangeRateViewSet
from django.urls import path, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'currencies', CurrencyViewSet)
router.register(r'currenciesExchanges', CurrencyExchangeRateViewSet)

urlpatterns = [
    path('', include(router.urls)),
]