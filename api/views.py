from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from datetime import datetime, timedelta
from api.serializers import CurrencySerializer, CurrencyExchangeRateSerializer, CurrencyExchangeRateDatesSerializer, \
    CurrencyExchangeRateAmountSerializer, CurrencyExchangeRateTimeWeightedSerializer
from api.models import Currency, CurrencyExchangeRate
from api.tasks import get_exchange_rate_data
from django.db.models import Avg
import random
# Create your views here.

# This part usually goes on utils file
def random_color():
    return "#{:06x}".format(random.randint(0, 0xFFFFFF))


class CurrencyViewSet(viewsets.ModelViewSet):
    """
    CurrencyViewset with all ALLOWED METHOD
    """
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer


class CurrencyExchangeRateViewSet(viewsets.ModelViewSet):
    """
    CurrencyViewset with all ALLOWED METHOD
    """
    queryset = CurrencyExchangeRate.objects.all()
    serializer_class = CurrencyExchangeRateSerializer

    @action(detail=False, methods=['post'])
    def beetweenDates(self, request):
        req = CurrencyExchangeRateDatesSerializer(data=request.data)
        if(req.is_valid()):
            curr = req.validated_data['source_currency']
            date_from = req.validated_data['date_from']
            date_to = req.validated_data['date_to']
            currencies = CurrencyExchangeRate.objects.filter(
                source_currency__code=curr, valuation_date__range=(date_from, date_to))
            result = CurrencyExchangeRateSerializer(currencies, many=True)
            return Response(result.data, status=status.HTTP_200_OK)
        else:
            return Response(req.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def calculate_amount(self, request):
        req = CurrencyExchangeRateAmountSerializer(data=request.data)
        if(req.is_valid()):
            curr = req.validated_data['source_currency']
            amount = req.validated_data['amount']
            ecurr = req.validated_data['exchanged_currency']
            exchanges = CurrencyExchangeRate.objects.filter(
                source_currency__code=curr, exchanged_currency__code=ecurr).order_by('-valuation_date')
            change_value = 'No enught data'
            if(len(exchanges) > 0):
                change_value = amount*exchanges[0].rate_value
            records = CurrencyExchangeRateSerializer(exchanges, many=True)
            return Response({"source_currency": curr, "exchanged_currency": ecurr, "invested": amount, "changed": change_value}, status=status.HTTP_200_OK)
        else:
            return Response(req.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['post'])
    def time_weighted(self, request):  # tasa de rendimeinto
        req = CurrencyExchangeRateTimeWeightedSerializer(data=request.data)
        if(req.is_valid()):
            now = datetime.datetime.now().strftime('%Y-%m-%d')
            curr = req.validated_data['source_currency']
            amount = req.validated_data['amount']
            ecurr = req.validated_data['exchanged_currency']
            start_date = req.validated_data['start_date']
            rates = CurrencyExchangeRate.objects.filter(
                source_currency__code=curr, exchanged_currency__code=ecurr, valuation_date__range=(start_date, now)).order_by('valuation_date')
            # final amount calc prevision ("Not realistic, only estimed")
            partial = 1000
            for rat in rates:
                partial = partial + amount*rat.rate_value
            return_invest = "not available"
            if(len(rates) > 1):
                return_invest = ((partial - amount)/amount)*100
            else:
                return_invest = 0
            revenue = partial - amount
            return Response({"source_currency": curr, "exchanged_currency": ecurr, "invested": amount, "TR": "{0:.2f} %".format(return_invest), "revenue": revenue}, status=status.HTTP_200_OK)
        else:
            return Response(req.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, methods=['get'])
    def time_serie(self, request):
        year = datetime.now().year
        prev_year = year - 5
        datasets=[]
        labels=[]
        for i in range(prev_year+1, year+1): 
            labels.append(i)
        currencies = Currency.objects.order_by('code').values_list('code', flat=True).distinct()
        for curr in currencies:
            color = random_color()
            exchanges=[]
            for i in range(prev_year+1, year+1):
                ratio = CurrencyExchangeRate.objects.filter(valuation_date__year=i,source_currency__code=curr).order_by('valuation_date').values_list('rate_value', flat=True).aggregate(avg=Avg('rate_value'))
                if(ratio['avg'] is None):
                    ratio['avg']=0
                exchanges.append(ratio['avg'])
            datasets.append({"label": ''.join([curr]),"backgroundColor": color ,"data": exchanges})
            
        var = {
            "labels": labels,
            "datasets": datasets
        }
        return Response(var, status=status.HTTP_200_OK)
